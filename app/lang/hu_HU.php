<?php

return array(
    'welcome' => 'Üdvözöllek, ez egy teszt üzenet!',
    'A user was found to match all plain text credentials however hashed credential  did not match.' => 'Belépés sikertelen!',
    'The password attribute is required.' => 'Hiányos adatok!',
    'The  attribute is required.' => 'Hiányos adatok!',
    'A user could not be found with a login value of .' => 'Belépés sikertelen!',
    'User  has been suspended.' => 'Felhasználó felfüggesztve!',
    'User  has been banned.' => 'Felhasználó letiltva!',
);

?>