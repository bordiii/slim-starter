<?php

$config['twig'] = array(
    'debug' => true,
    'cache' => APP_PATH. 'storage/cache',
    'language' => 'hu_HU'
);