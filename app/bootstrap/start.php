<?php

session_cache_limiter(false);
session_start();

define('ROOT_PATH'  , __DIR__.'/../../');
define('VENDOR_PATH', __DIR__.'/../../vendor/');
define('APP_PATH'   , __DIR__.'/../../app/');
define('MODULE_PATH', __DIR__.'/../../app/modules/');
define('PUBLIC_PATH', __DIR__.'/../../public/');

use \Symfony\Component\Translation\Translator as Translator;

require VENDOR_PATH.'autoload.php';

/**
 * Load the configuration
 */
$config = array(
    'path.root'     => ROOT_PATH,
    'path.public'   => PUBLIC_PATH,
    'path.app'      => APP_PATH,
    'path.module'   => MODULE_PATH
);

foreach (glob(APP_PATH.'config/*.php') as $configFile) {
    require $configFile;
}

/** Merge cookies config to slim config */
if(isset($config['cookies'])){
    foreach($config['cookies'] as $configKey => $configVal){
        $config['slim']['cookies.'.$configKey] = $configVal;
    }
}

/**
 * Initialize Slim and SlimStarter application
 */
$app        = new \Slim\Slim($config['slim']);
$starter    = new \SlimStarter\Bootstrap($app);

$starter->setConfig($config);

// First param is the "default language" to use.
$translator = new Translator($config['twig']['language'], new \Symfony\Component\Translation\MessageSelector());
// Set a fallback language incase you don't have a translation in the default language
$translator->setFallbackLocales(['hu_HU']);
// Add a loader that will get the php files we are going to store our translations in
$translator->addLoader('php', new \Symfony\Component\Translation\Loader\PhpFileLoader());
// Add language files here
$translator->addResource('php', APP_PATH . 'lang/hu_HU.php', 'hu_HU'); // Hungarian
$translator->addResource('php', APP_PATH . 'lang/en_GB.php', 'en_GB'); // English

/**
 * if called from the install script, disable all hooks, middlewares, and database init
 */
if(!defined('INSTALL')){
    /** boot up SlimStarter */
    $starter->boot();

    /** Setting up Slim hooks and middleware */
    require APP_PATH.'bootstrap/app.php';

    /** registering modules */
    foreach (glob(APP_PATH.'modules/*') as $module) {
        $className = basename($module);
        $moduleBootstrap = "\\$className\\Initialize";

        $app->module->register(new $moduleBootstrap);
    }

    $app->module->boot();

    /** Start the route */
    require APP_PATH.'routes.php';
}else{
    /** disregard sentry configuration on install */
    $config['aliases']['Sentry'] = 'Cartalyst\Sentry\Facades\Native\Sentry';

    $starter->bootFacade($config['aliases']);
}

return $starter;