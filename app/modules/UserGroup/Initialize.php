<?php

namespace UserGroup;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'UserGroup';
    }

    public function getModuleAccessor(){
        return 'usergroup';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $userGroup = $adminMenu->createItem('admingroup', array(
            'label' => $this->translator->trans("Adminisztrátorok"),
            'icon'  => 'group',
            'url'   => '#'
        ));
        $userGroup->setAttribute('class', 'nav nav-second-level');

        $userMenu = $adminMenu->createItem('user', array(
            'label' => $this->translator->trans("Admin felhasználók"),
            'icon'  => 'user-secret',
            'url'   => 'admin/user'
        ));

        $userGroup->addChildren($userMenu);

        $adminMenu->addItem('admingroup', $userGroup);
    }

    public function registerAdminRoute(){
        Route::resource('/user', 'UserGroup\Controllers\UserController');
    }
}